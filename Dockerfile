FROM chandrantwins/nuxeo-ffmpeg:latest
RUN apt-get update && apt-get install -y netcat-openbsd
ADD init-nuxeo-cluster-node.sh /docker-entrypoint-initnuxeo.d/init-nuxeo-cluster-node.sh